const glob = require('glob')
const EventEmitter = require('events')
const { sizeof } = require('sizeof')
const defaultDirectory = process.cwd()
const excludeTestFiles = (filename) => !/.*\.(spec|test).js$/.test(filename) && !/.*__mocks__.*\.js$/.test(filename)

/**
 * Loads all valid components within the directory with config files and references
 * @param {string} options.directory: directory containing components - defaults to __directory/src/components
 * @param {string} options.configDirectory: directory containing config files - defaults to __directory/config
 * @returns {array} list of components with their props, functions and config files
 */
const loadContainer = async (options = {}) => {
  const { directory, configDirectory, init } = defaultOptions (options)

  // Components
  const components = loadDirectoryComponents({ directory, configDirectory })
  
  // Init
  const order = getDependencyOrder(components)
  
  if (!init) return createContainer(components)
  
  for (let name of [...order]) {
    await initComponent(components.find(itm => itm.name === name))
  }
  return createContainer(components)
}

/**
 * Loads a component - with dependencies 
 * @param {string} options.directory: directory containing components - defaults to __directory/src/components
 * @param {string} options.configDirectory: directory containing config files - defaults to __directory/config
 * @returns {array} list of components with their props, functions and config files
 */
const loadComponent = async (comp, options = {}) => {
  const { directory, configDirectory, init } = defaultOptions (options)

  if (!isValidComponent(comp)) throw Error(`Invalid component`)

  const components = loadDirectoryComponents({ directory, configDirectory })
  const order = getDependencyOrder(components, components.filter(({ name }) => name === comp.name))
  
  if (!init) return createContainer(components.filter(({name}) => [...order].includes(name)))

  for (let name of [...order]) {
    await initComponent(components.find(itm => itm.name === name))
  }
  return createContainer(components.filter(({name}) => [...order].includes(name)))
}

const defaultOptions = ({
  directory = `${defaultDirectory}/src/components`,
  configDirectory = (directory ? `${directory}/../../config` : `${defaultDirectory}/config`),
  init = true
}) => ({ directory, configDirectory, init })

const initComponent = async (comp) => {
  if (comp.init) await comp.init()
}

/**
 * Returns valid components from the specified directory
 * - Loads all js modules
 * - Filters out tests and invalid components
 * - Create components objects and binds functions
 */
const loadDirectoryComponents = ({ directory, configDirectory }) => {
  return glob.sync(`${directory}/**/*.js`)
  .filter(excludeTestFiles)
  .map(require)
  .filter(isValidComponent)
  .map(itm => createComponent(itm, { directory, configDirectory }))
  .map(referenceDeps)
}

/**
 * Adds reference from a component dependencies
 * @param {*} component: valid component
 * @param {*} availableComponents: all available components
 */
const referenceDeps = (component, index, availableComponents) => {
  if (!component.deps) return component
  component.deps.forEach(dep => {
    const depMatches = availableComponents.filter(({ name }) => name === dep)
    if (depMatches.length < 1) throw Error(`Missing dependency '${dep}' referenced in component '${component.name}'`)
    if (depMatches.length > 1) throw Error(`More than 1 components are named '${dep}'`)
    component[dep] = depMatches[0]
  })
  return component
}

const isValidComponent = (component) => component && component.name

/**
 * Turns jsModule into formatted component with config files and bound functions
 * @param {*} jsModule 
 * @param {*} options.configDirectory: path to config files 
 */
const createComponent = (jsModule, options = {}) => {
  const { configDirectory, withDependencies } = defaultOptions(options)
  if (withDependencies) throw Error('Cannot load dependencies when using createComponent, use loadComponent with withDependencies option instead')
  if (!jsModule.name) throw Error('Component must have a name')
  
  return bindComponentFunctions({
    deps: [],
    config: loadConfig({ jsModule, configDirectory }),
    emitter: new EventEmitter(),
    ...jsModule
  })
}

/**
 * Binds functions to the component and leaves other component properties as is
 * @param {*} comp: valid component object
 */
const bindComponentFunctions = (comp) => 
  Object.entries(comp).reduce((acc, [key, prop]) => {
    acc[key] = typeof prop === 'function'
      ? prop.bind(acc)
      : prop
      return acc
  }, {})

const loadConfig = ({ jsModule, configDirectory }) => {
  try {
    return require(`${configDirectory}/${jsModule.name}`)
  } catch (e) {
    if (e.message.startsWith('Cannot find module')) return
    throw Error(`Error in config file ${jsModule.name}, ${e.message}`)
  }
}

/**
 * Packs components into a container with boilerplate functions
 * @param {*} components 
 */
const createContainer = (components) => {
  return {
    registry: components,
    ...components.reduce((acc, itm) => ({
      ...acc,
      [itm.name]: itm
    }), {}),
    info () {
      return sizeof(this)
    },
    get (name) {
      return this[name]
    },
    length: components.length
  }
}

/**
 * Returns the order of dependencies starting with components with no dependencies
 * @param {array} registry: All components available in the directory.
 * @param {array} components: Limited set of components which are to be returned along their dependencies.
 */
const getDependencyOrder = (registry, components = registry) => {
  /**
   * Recursively finds components dependencies and returns them in the correct order 
   * @param {*} list: The list of components which dependencies have to be ordered
   * @param {*} order: order from components with no deps.
   */
  const orderDeps = (list, order = []) => {
    try {
      return list.map(({ name, deps }) => {
        if (deps.length) order = orderDeps(deps.filter(dep => !order.includes(dep)).map(dep => registry.find(itm => itm.name === dep)), order)
        return order.concat(name)
      }).flat()
    } catch (e) {
      if (e.message.startsWith('Maximum call stack size exceeded')) throw new Error(`Circular dependency detected`)
      throw e
    }
  }
  return new Set(orderDeps(components))
}

module.exports = {
  loadContainer,
  loadComponent,
  createComponent
}
