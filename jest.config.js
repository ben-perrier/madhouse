/**
 * @see https://jestjs.io/docs/en/configuration
 */

module.exports = {
  // verbose: true,
  testMatch: ['**/tests/*test.js']
}
