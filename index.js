const { loadContainer, loadComponent, createComponent } = require('./src')

module.exports = {
  loadContainer,
  loadComponent,
  createComponent
} 