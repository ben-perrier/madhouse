# Madhouse js components

Madhouse is a simple framework for structuring Node.js applications in object components.

[Component] A component is a JS object with a `name` property and optional dependencies `deps` and `init` function.
[Container] A container groups components together and manages the `deps` graph initialisation cycle.

## Meet the component

A component is a JS module returning an object. It's up to you what properties your component exposes, except for the following reserved properties (\* = required):

- `name` (\*): Unique component identifier, will be used as namespace to access this component's properties from other components referencing this one in the `deps`.
- `deps`: Array of components names which need to be initialised before this one, so their properties can be accessed by this component.
- `init`: Function that needs to run when initialising this component.
- `config`: component `config` is automatically added to the components when loading a container from config files. A config file name matches that of the component name and is placed in the configDirectory.
- [`any_other_property`]: You decide how to break down your component so it makes sense to you!

```javascript
// Npm package import
const Server = require("your-server-package");

// Private object
const appServer = new Server(this.config.serverConfig);

module.exports = {
  // Unique name referred to in other components
  name: "server",

  // Dependent components which need to be initialised beforehand
  deps: ["usersModel"],

  // Init function
  init() {
    appServer.get("/user", this.usersModel.get);
    appServer.post("/user", this.usersModel.update);
  },

  // Public property which can be accessed by other components
  async startServer() {
    appServer.start();
  },
};
```

## Meet the container

A container is a JS object initialised just once, typically when running `npm start`

- `directory`: Top-level folder to look out for components. Madhouse will reference as a component any js module returning an object with a unique `name` property. It will exclude any test file ending in `.spec.js`, `.test.js` or in a `__mocks__` folder.
- `configDirectory`: Top-level folder to look out for config files. Madhouse will initialise components which name matches the config file name.

`src/index.js`

```javascript
const { loadContainer } = require('madhouse')

const app = () => {
  const start = new Date()

  // Create your container
  const container = await loadContainer({
    directory: `${__dirname}/src/components`, // Default path
    configDirectory: `${__dirname}/config`,   // Default path
  })

  // Once the containe rwith all components is loaded, you can access component functions.
  await container.server.startServer()
  console.log(`Container loaded in ${new Date() - start} ms`)
}

app()
```

Config file in **root_directory**/config/server.js

```javascript
module.exports = {
  serverConfig: {
    host: "users.madhouse.com",
    port: 8080,
  },
};
```

## Usage

### loadContainer - async function

Use loadContainer to create and start an application.
It will look up all valid components within the `directory`, load components with their config files in `configDirectory` and start components by order of dependency.

| param1: options   | description                                             | default                 |
| ----------------- | ------------------------------------------------------- | ----------------------- |
| `directory`       | Absolute path to the folder containing the components   | `<root>/src/components` |
| `configDirectory` | Absolute path to the folder containing the config files | `<root>/config`         |

Example:

```javascript
const { loadContainer } = require('madhouse')

const app = () => {
  const start = new Date()
  const container = await loadContainer({
    directory: `${__dirname}/src/components`, // Default path
    configDirectory: `${__dirname}/config`,   // Default path
  })
  await container.server.startServer()
  console.log(`Container loaded in ${new Date() - start} ms`)
}

app()
```

### loadComponent - async function

Use loadComponent to load a component and its dependencies - this is useful to test a component's behaviour.
It will return a container which includes the component and dependencies.

- param1: jsModule
- param2: options

| options           | description                                             | default                 |
| ----------------- | ------------------------------------------------------- | ----------------------- |
| `directory`       | Absolute path to the folder containing the components   | `<root>/src/components` |
| `configDirectory` | Absolute path to the folder containing the config files | `<root>/config`         |

Example

```javascript
const { loadComponent } = require("madhouse");

let container = null;

beforeAll(async () => {
  container = await loadComponent(
    require("./working-app/src/components/usersModel"),
    {
      directory: `${__dirname}/src/components`, // Default path
      configDirectory: `${__dirname}/config`, // Default path
    }
  );
});

test("usersModel and dependencies are loaded correctly", async () => {
  const { usersModel, db } = container;
  expect(typeof usersModel).toBe("object");
  expect(typeof db).toBe("object");
  // Deps are referenced
  expect(typeof usersModel.db).toBe("object");
  // Config is loaded
  expect(typeof db.config).toBe("object");
  expect(typeof db.config.dbConnection.host).toBe("string");
});
```

### createComponent - sync function

Use createComponent to load a single component without its dependencies.

- param1: jsModule
- param2: options

| options           | description                                             | default         |
| ----------------- | ------------------------------------------------------- | --------------- |
| `configDirectory` | Absolute path to the folder containing the config files | `<root>/config` |

Example:

```javascript
const { createComponent } = require("madhouse");

const db = createComponent(require("./working-app/src/components/db"), {
  configDirectory: `${__dirname}/working-app/config`,
});

test("usersModel has a ref to dependency db", async () => {
  expect(typeof db).toBe("object");
  expect(typeof db.config).toBe("object");
});
```
