const { loadContainer } = require('../../src')

const container = (async () => {
  const container = await loadContainer({ 
    directory:`${__dirname}/src/components`,
    configDirectory:`${__dirname}/config`
  })
  container.get('server').startServer()
  return container
})()

module.exports = container