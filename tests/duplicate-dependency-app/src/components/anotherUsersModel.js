const dbClient = { isConnected: false, connect: (config) => new Promise((resolve) => { this.connected = true; resolve('connected') }), on: ()=>{}}

module.exports = {
  name: 'usersModel',

  async init () {
    this.config.dbConfig  // is available
    this.client = dbClient
    await this.connect()
    this.client.on('disconnect', () => this.connect())
  },

  async connect () {
    await this.client.connect(this.config.dbConfig)
  }
  
}