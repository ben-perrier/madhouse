const { loadComponent } = require("../index");

describe("loadComponent with init", () => {
  let container = null;

  beforeAll(async () => {
    container = await loadComponent(
      require("./working-app/src/components/usersModel"),
      {
        directory: `${__dirname}/working-app/src/components`,
        configDirectory: `${__dirname}/working-app/config`,
      }
    );
  });

  it("contains the referenced component and its dependencies", async () => {
    expect(typeof container.usersModel).toBe("object");
    expect(typeof container.db).toBe("object");
  });

  it("does not contain other components", () => {
    expect(container.server).toBeUndefined();
  });

  it("has referenced deps in components", () => {
    expect(typeof container.usersModel.db).toBe("object");
  });

  it("has loaded config files deps in components", () => {
    expect(typeof container.db.config).toBe("object");
    expect(typeof container.db.config.dbConnection.host).toBe("string");
  });
});
