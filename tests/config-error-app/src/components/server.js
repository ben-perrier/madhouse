const server = { isUp: false, start: () => new Promise((resolve) => { this.isUp = true; resolve() }) }

module.exports = {
  name: 'server',
  deps: ['usersModel'],

  init () {
    this.server = server
    this.addRoutes()
  },

  addRoutes () {
    return this.server.routes = { getUser: this.usersModel.get }
  },

  async startServer () {
    return this.server.start()
  }
}