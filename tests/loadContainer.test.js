const app = require('./working-app')

describe('loadContainer', () => {

  it('should return a container with 3 components', async () => {
    const container = await app
    const { usersModel, server, db, length } = container
    expect(length).toBe(3)
    expect(typeof usersModel).toBe('object')
    expect(typeof server).toBe('object')
    expect(typeof db).toBe('object')
  })

  describe('loadContainer components', () => {
    it('should include references to components dependencies', async () => {
      const container = await app
      const { usersModel, server } = container
      expect(typeof usersModel.db).toBe('object')
      expect(typeof server.usersModel).toBe('object')
    })

    it('should have config file properties loaded', async () => {
      const container = await app
      const { db } = container
      expect(typeof db.config).toBe('object')
      expect(typeof db.config.dbConnection.host).toBe('string')  
    })
  })

  describe('loadContainer errors', () => {
    it('should detect circular references', async () => {
      const circularDependencyApp = require('./circular-dependency-app')
      try {
        await circularDependencyApp
      } catch (e) {
        expect(e.message).toBe('Circular dependency detected')
      }
    })
  
    it('should detect errors in config files', async () => {
      const configErrorApp = require('./config-error-app')
      try {
        await configErrorApp
      } catch (e) {
        expect(e.message).toBe('Error in config file db, undefinedVariable is not defined')
      }
    })
  
    it('should error if a component is missing a name', async () => {
      const missingDependencyApp = require('./missing-dependency-app')
      try {
        await missingDependencyApp
      } catch (e) {
        expect(e.message).toBe(`Missing dependency 'db' referenced in component 'usersModel'`)
      }
    })
  
    it('should error if several components have the same name', async () => {
      const duplicateDependencyApp = require('./duplicate-dependency-app')
      try {
        await duplicateDependencyApp
      } catch (e) {
        expect(e.message).toBe(`More than 1 components are named 'usersModel'`)
      }
    })
  })

})
