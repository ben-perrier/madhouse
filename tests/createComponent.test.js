const { createComponent } = require('../src/index')

test('usersModel has a ref to dependency db', async () => {
  const db = createComponent(require('./working-app/src/components/db'), { 
    configDirectory: `${__dirname}/working-app/config` 
  })
  expect(typeof db).toBe('object')
  expect(typeof db.config).toBe('object')
})