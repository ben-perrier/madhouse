module.exports = {
	name: 'db',

	init: jest.fn(),

	connect: jest.fn(),

	executeQuery: jest.fn()
	
}